//sub class that have overriding abstract methods
public class Anime extends MovieCollecion{

	//subclass override default constructor 
	public Anime(String string,int price) {
		super(string,price);
		
	}
	
	
	@Override
	public String play(){
		return "Anime playing !";
	}
		
	@Override
	public int ticketPrice(int price){
		return price;
	}
	

}
