//sub class that have overriding abstract methods
public class Series extends MovieCollecion {

	//subclass override default constructor 
	public Series(String string,int price) {
		super(string,price);
	}
	
	
	@Override
	public String play(){
		return "Series playing !!";
	}
	
	@Override
	public int ticketPrice(int price){
		return price;
	}

}
