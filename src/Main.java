import java.util.ArrayList;


public class Main {


	public static void main(String[] args) {
		Anime anime = new Anime("KUROKO NO BASUKE",120);
		Series series = new Series("Prison Break",150);
		
		System.out.println(anime);
		System.out.println(series);
		
		ArrayList<MovieCollecion> collection = new ArrayList<MovieCollecion>();
		collection.add(anime);
		collection.add(series);
		
		for (MovieCollecion collect: collection){
			System.out.println(collect.play());
		}
		
		Theater thea = new Theater();
		System.out.println(thea.makePlay(anime));
		System.out.println(thea.makePlay(series));
		System.out.println(thea.makePlay((MovieCollecion) anime));
		System.out.println(thea.makePlay((MovieCollecion) series));
		
		System.out.println(anime+" tiket price in Wednesday");
		System.out.println(thea.getTKPrice(anime.getPrice(),"Wednesday"));
		System.out.println(anime+" tiket price in Monday");
		System.out.println(thea.getTKPrice(anime.getPrice(),"Monday"));
		
		System.out.println(series+" tiket price in Wednesday");
		System.out.println(thea.getTKPrice(series.getPrice(),"Wednesday"));
		System.out.println(series+" tiket price in Thursday");
		System.out.println(thea.getTKPrice(series.getPrice(),"Thursday"));

	}

}
