//Super Class
public abstract class MovieCollecion {
	private String name;
	private int price;

	public MovieCollecion(String name,int price) {
		this.name=name;
		this.price=price;
	}
	
	//abstract methods
	public abstract String play();
	
	public abstract int ticketPrice(int price);
	
	public String toString(){
		return this.name;
	}
	
	public int getPrice(){
		return this.price;
	}

}
